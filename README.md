## Description

Nest is a framework for building efficient, scalable <a href="http://nodejs.org" target="_blank">Node.js</a> server-side applications. It uses modern JavaScript, is built with  <a href="http://www.typescriptlang.org" target="_blank">TypeScript</a> (preserves compatibility with pure JavaScript) and combines elements of OOP (Object Oriented Programming), FP (Functional Programming), and FRP (Functional Reactive Programming).

<p>Under the hood, Nest makes use of <a href="https://expressjs.com/" target="_blank">Express</a>, but also, provides compatibility with a wide range of other libraries, like e.g. <a href="https://github.com/fastify/fastify" target="_blank">Fastify</a>, allowing for easy use of the myriad third-party plugins which are available.</p>

## Running The Application

1. Creating Redis image on your local machine you can run `docker-compose up -d`
2. For running the application on local machine, you can also run `cp .env.example .env`
3. Installing nestjs cli: `npm i -g @nestjs/cli`
4. Installing dependencies: `npm install`
5. Start the server: `npm start`
6. Run integration test: `npm run test:e2e`

### Some Considerations:
- Unfortunately, I faced with issues to retrieve data from FBI API so I just used mock data which were in `sample_data`
  to represent data.
- There is a code comment in `ws.guard.ts` file which I didn't remove intentionally for issuing tokens, if you want to test with a new token you can uncomment this part.

# 1 Websocket API

## 1.1 Websocket API in NodeJS

There is `socket.test.html` in which you can test the connection via WebSocket after each connection

`auth`: for saving the socket ids in Redis,

`get-wanteds`: you can get the list of wanteds asynchronously, also it accepts payload for pagination like `{ page: 1, limit: 10 }`.

`get-wanted`: you can get one single wanted asynchronously it accepts payload for pagination like `{ id: 'qqeebxbmbxmss' }`.

## 1.2 Docker image

There is a `Dockerfile` for making an image of the application.

## 1.3 Kubernetes

In `deployment` file I have added the deployment description for Kubernetes which is related to the production image.

## 1.4 Automation of local development workflow

I added `bitbucket-pipeline` file which is used to build the image and push it on Docker hub,

for each develop, staging, and production server there is a specific branch with the below mapping for tags:

1. latest: develop branch
2. release-latest: release/* branch (* means any version)
3. prd-latest: master branch

# 2 Caching

I added Redis connection to cache data per each specific page and limit for a particular time.

# 3 Event subscriptions

For this purpose I'm setting and mapping the user ids to their socket ids because in future we want to know which user has subscribed for which event

after that, we want to notify the user so we want to `socketId` on that time.

Also, we can make requests frequently(like every 3 second) to FBI API and compare it with our cached data to check its changed or not

in case of any change, we can search for the users which have subscribed for that event or change and inform them by their socket ids.

# 4 Persistent caching

For this purpose, we can have a background job to run and persist the cached data in for example Mysql DB

I have created to `wanted` entity already to show the table in which we want to persist our data.

# 5 Security

I added JWT authentication to WS services which you can find in `socket.test.html` file,

for this purpose I'm getting advantages of Nestjs guard decorators which are used to validate tokens in the request header.

# 6 Logging

Yes we need to log some important data with getting advantages of Nestjs logger, and also  by using some

third parties like Datadog or ELK  which have integration with Nestjs logger module.

Also, for error handling, we can use Sentry service which is specifically created for better error reporting

and monitoring.

# 7 Scaling

By adding replicas in our deployment file we can scale the application horizontally.
