FROM node:14-alpine

WORKDIR /app

ADD package.json /app
RUN npm install

ADD . /app

RUN npm run build

CMD ["npm", "run", "start:prod"]
