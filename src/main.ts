import { NestFactory }         from '@nestjs/core';
import { AppModule }           from './app.module';
import { AllExceptionsFilter } from './common/exceptions/all-exceptions.filter';
import { configService } from './config.service';
import { RedisIoAdapter } from './redis-io.adaptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Catch all exceptions
  app.useGlobalFilters(new AllExceptionsFilter());

  app.useWebSocketAdapter(new RedisIoAdapter(app));

  await app.listen(configService.get<number>('SERVICE_PORT'));

}

bootstrap();
