import { Injectable } from '@nestjs/common';
import { SocketGateway } from './socket.gateway';
import { ModuleRef } from '@nestjs/core';
import { configService } from '../config.service';
import * as redis from 'redis';
import * as asyncRedis from 'async-redis';
import { WantedsService } from '../wanteds/wanteds.service';
import PaginationDto from '../common/dto/pagination.dto';

@Injectable()
export class SocketService {

  readonly redisClient;

  constructor(
    private readonly socketGateway: SocketGateway,
    private readonly wantedsService: WantedsService,
    private readonly moduleRef: ModuleRef
  ){
    this.redisClient = redis.createClient({
      port: configService.get<number>('REDIS_PORT'),
      host: configService.get<string>('REDIS_HOST'),
      password: configService.get<string>('REDIS_PASSWORD')
    });
    this.redisClient = asyncRedis.decorate(this.redisClient);
  }

  /**
   *
   * @param userId
   * @param socketId
   */
  public async setSocket(userId, socketId) {
    await this.redisClient.rpush(userId, socketId);
  }

  /**
   *
   * @param userId
   * @param socketId
   */
  public async removeSocket(userId, socketId) {
    let allSocketIds = await this.redisClient.lrange(userId, 0, 100);
    allSocketIds = allSocketIds.filter(preSocketId => preSocketId === socketId);
    if (allSocketIds.length)
      await this.redisClient.rpush(userId, allSocketIds);
  }

  /**
   *
   * @param userId
   * @param socketId
   */
  public async getWantedsCall(paginationDto: PaginationDto) {
    const targetKey = `page:${paginationDto.page},limit:${paginationDto.limit}`;
    const cachedData = await this.redisClient.get(targetKey);
    if (cachedData) {
      // Reading from cache.
      const data = JSON.parse(cachedData);
      return data;
    } else {
      // Reading from service.
      try {
        const data = await this.wantedsService.getWanteds(paginationDto);
        await this.redisClient.set(
            `page:${paginationDto.page},limit:${paginationDto.limit}`,
            JSON.stringify(data),
            'EX',
            '60'
        );
        return data;
      } catch (e) {
        // there is an error  with service, reading from cache again.
        const cachedData = await this.redisClient.get(targetKey);
        const data = JSON.parse(cachedData);
        return data;
      }
    }
  }

  /**
   *
   * @param id
   */
  public async getWantedCall(id: string) {
    return this.wantedsService.getWanted(id);
  }

}
