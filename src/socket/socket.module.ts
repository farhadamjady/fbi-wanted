import { Module } from '@nestjs/common';
import { SocketService } from './socket.service';
import { SocketGateway } from './socket.gateway';
import { WantedsModule } from "../wanteds/wanteds.module";

@Module({
  imports: [
    WantedsModule
  ],
  providers: [SocketService, SocketGateway],
  exports: [SocketService]
})
export class SocketModule {}
