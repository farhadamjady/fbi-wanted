import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { UseGuards } from '@nestjs/common';
import { configService } from '../config.service';
import { WsJwtGuard } from '../auth/ws.guard';
import { SocketService } from './socket.service';
import { ModuleRef } from '@nestjs/core';
import PaginationDto from '../common/dto/pagination.dto';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@WebSocketGateway(configService.get<number>('SERVICE_PORT'), { transports: ['websocket'] })
export class SocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  private socketService: SocketService;

  @WebSocketServer() server: Server;

  constructor(
    private readonly moduleRef: ModuleRef
  ) {}

  /**
   *
   */
  onModuleInit(): any {
    this.socketService = this.moduleRef.get(SocketService, { strict: false });
  }

  /**
   *
   * @param server
   */
  afterInit(server: Server) {
    console.log('Socket Initiated');
  }

  /**
   *
   * @param client
   */
  @UseGuards(WsJwtGuard)
  async handleDisconnect(client: any) {
    await this.socketService.removeSocket(client.userId, client.id);
  }

  /**
   *
   * @param client
   * @param args
   */
  handleConnection(client: Socket, ...args: any[]) {
  }

  /**
   *
   * @param client
   */
  @UseGuards(WsJwtGuard)
  @SubscribeMessage('auth')
  async setAuthentication(client: any) {
    await this.socketService.setSocket(client.userId, client.id);
    return {};
  }

  /**
   *
   * @param client
   * @param paginationDto
   */
  @UseGuards(WsJwtGuard)
  @SubscribeMessage('get-wanteds')
  async getWanteds(client: any, paginationDto: PaginationDto): Promise<Observable<WsResponse<any>>> {
    const data = await this.socketService.getWantedsCall(paginationDto);
    const event = 'get-wanteds';
    return from(data).pipe(
        map(data => ({ event, data })),
    );
  }

  @UseGuards(WsJwtGuard)
  @SubscribeMessage('get-wanted')
  async getWanted(client: any, payload: any): Promise<Observable<WsResponse<any>>> {
    const data = await this.socketService.getWantedCall(payload.id);
    const event = 'get-wanted';
    return from(data).pipe(
        map(data => ({ event, data })),
    );
  }
}
