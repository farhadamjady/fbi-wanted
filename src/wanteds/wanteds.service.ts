import { Injectable } from '@nestjs/common';
import PaginationDto from "../common/dto/pagination.dto";
import { fetchWantedsByURL, fetchWantedByURL } from '../utils/fetch.util';

@Injectable()
export class WantedsService {

  /**
   *
   * @param postId
   */
  public async getWanteds(paginationDto: PaginationDto) {
    return await fetchWantedsByURL(paginationDto);
  }

  /**
   *
   * @param paginationDto
   */
  public async getWanted(id: string) {
    return await fetchWantedByURL(id);
  }

}
