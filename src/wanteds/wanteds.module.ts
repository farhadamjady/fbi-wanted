import { forwardRef, Module } from '@nestjs/common';
import { WantedsService } from './wanteds.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SocketModule } from '../socket/socket.module';

@Module({
  imports: [
    forwardRef(() => SocketModule)
  ],
  providers: [WantedsService],
  exports: [WantedsService]
})
export class WantedsModule {}
