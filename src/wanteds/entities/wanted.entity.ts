import {
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm';

@Entity({
  name: 'wanteds'
})
export class Wanted {

  @PrimaryGeneratedColumn()
  id: number;

}

