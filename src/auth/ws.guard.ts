import { CanActivate, Injectable } from '@nestjs/common';
import { JwtService }    from '@nestjs/jwt';
import { configService } from '../config.service';

@Injectable()
export class WsJwtGuard implements CanActivate {

  private readonly jwtService: JwtService;

  constructor() {
    this.jwtService = new JwtService( { secret: configService.get<string>('ACCESS_TOKEN_SECRET_KEY') });
  }

  async canActivate(context): Promise<any> {
    const request = context.switchToWs().getClient();
    const authToken: string = request.handshake.query.authorization.split(' ')[1];
    // const token = await this.jwtService.sign({ id: 1 })
    // console.log( 'token', token );
    try {
      const decoded = this.jwtService.verify(authToken) as any;
      return new Promise((resolve, reject) => {
        if (decoded) {
          request.userId = decoded.id;
          resolve(decoded);
        } else
          reject(false);
      });
    } catch (ex) {
      return false;
    }
  }
}
