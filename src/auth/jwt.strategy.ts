import { ExtractJwt, Strategy }              from 'passport-jwt';
import { PassportStrategy }                  from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import JwtPayloadInterface                   from './interfaces/jwtPayload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.ACCESS_TOKEN_SECRET_KEY,
    });
  }

  async validate(payload: JwtPayloadInterface) {
    if (!payload || !payload.id) {
      return false; // you can also throw UnauthorizedException
    }

    try {
      return payload;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
}
