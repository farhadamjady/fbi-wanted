import {configService} from "../config.service";
import PaginationDto from "../common/dto/pagination.dto";
const fs = require('fs');
const path = require('path');
const items = JSON.parse(fs.readFileSync(path.join(__dirname, '../../', 'mockData/items.json')));
const fetch = require('node-fetch');
const Bluebird = require('bluebird');

fetch.Promise = Bluebird;

export const fetchWantedsByURL = (paginationDto: PaginationDto) => {
    const page = paginationDto.page;
    const limit = paginationDto.limit;
    return items.items.slice(limit * (page - 1), (page * limit) + 1);
    // return new Promise((resolve) => {
    //     fetch(
    //         configService.get<number>('FBI_URL') + '/wanted/v1/list/?' +
    //         'page=' + paginationDto.page + '&' +
    //         'limit=' + paginationDto.limit
    //     ).then((res) => {
    //         resolve(res.json());
    //     })
    // })
}

/**
 *
 * @param id
 */
export const fetchWantedByURL = (id: string) => {
    return items.items.filter((item) => {
        return item['@id'] == `https://api.fbi.gov/@wanted-person/${id}`;
    });
    // return new Promise((resolve) => {
    //     fetch(
    //         configService.get<number>('FBI_URL') + '/@wanted-person/' + id
    //     ).then((res) => {
    //         resolve(res.json());
    //     })
    // })
}
