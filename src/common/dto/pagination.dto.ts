import { IsNumber, IsObject, IsOptional, Max } from 'class-validator';
import { Exclude, Expose, Transform }          from 'class-transformer';

const defaultValueDecorator = (defaultValue: number) => {
  return Transform((target: any) => {
    return target && target > 0 ? target : defaultValue;
  });
};

@Exclude()
export default class PaginationDto {

  @Expose()
  @Transform(page => page ? parseInt(page) : 1)
  @IsNumber()
  @IsOptional()
  @defaultValueDecorator(1)
  page: number;

  @Expose()
  @Transform(limit => limit ? parseInt(limit) : 20)
  @IsNumber()
  @Max(20)
  @IsOptional()
  @defaultValueDecorator(20)
  limit: number;

}
