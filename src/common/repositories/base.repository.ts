import { Repository }      from 'typeorm/index';

export default class BaseRepository<T> extends Repository<T> {
}
