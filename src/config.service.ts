import { ConfigService }                from '@nestjs/config';
import { InternalServerErrorException } from '@nestjs/common';

require('dotenv').config();

class AppConfig extends ConfigService {
  /**
   *
   * @param env
   */
  constructor(private env: { [k: string]: string | undefined }) {
    super();
  }

  /**
   *
   * @param key
   * @param throwOnMissing
   */
  private getValue(key: string, throwOnMissing: boolean = true): any {
    const value = this.get(key);
    if (!value && throwOnMissing) {
      throw new InternalServerErrorException(`config error - missing env.${ key }`);
    }
    return value;
  }

  /**
   *
   * @param keys
   */
  public ensureValues(keys: string[]): AppConfig {
    keys.forEach(k => this.getValue(k, true));
    return this;
  }

}

const configService = new AppConfig(process.env)
  .ensureValues(['FBI_URL']);

export { configService };
