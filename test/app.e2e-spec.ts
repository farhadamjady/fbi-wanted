import { Test, TestingModule } from '@nestjs/testing';
import * as WebSocket from 'ws'
import { SocketModule } from '../src/socket/socket.module';
import { WsAdapter }    from '@nestjs/platform-ws';
import { INestApplication } from '@nestjs/common';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        SocketModule,
      ],
    }).compile()

    app = moduleFixture.createNestApplication();
    app.useWebSocketAdapter(new WsAdapter(app));
    await app.init();
  })

  it('should connect successfully', (done) => {
    const address = app.getHttpServer().listen().address();
    const baseAddress = `http://[${address.address}]:${address.port}`;

    const socket = new WebSocket(baseAddress)
    
    socket.on('open', () => {
      console.log( 'connected!' );
      // socket.emit('get-wanteds', { page: 1, limit: 10 });
      // socket.on('get-wanteds', function (data) {
      //   console.log( 'data', data );
      //   done();
      // });
      done();
    })

    socket.on('close', (code, reason) => {
      done({ code, reason })
    })

    socket.on ('error', (error) => {
      done(error)
    })
  })
})

